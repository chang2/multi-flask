# HTML & CSS

## I. Web
### 1. Web()

### 2. Web browser
- 우리를 대신해서 정보를 요청해주고,
- 받아온 응답을 예쁘게 보여준다.

## II. HTML
### 1. HTML이란
- HTML(HyperText Markup Language) : 링크를 걸 수 있는 
- Hypertext : 
- **Mark**up Language: 마크업 언어(특정 표시(mark)되어 있는 언어) 
- 

### 2. 
```html
<!DOCTYPE html>
<html>
    <head>

    </head>
    <body>

    </body>
</html>
```
### 2. 

### 3. 주의사항
- a. 띄어쓰기, 들여쓰기(Indentation)
    - 자유롭게 사용 가능
    - 1tab(2 spaces, 4 spaces)
- b. 태그는 
    - 예외도 있음

## II. 다양한 HTML 태그(tag)
### 1. <h1> : 제목(heading) 태그
```html
<h1><h1>
```

### 2. <p> : 글, 문단(paragraph) 태그

### 3. <ul>
```html
<ul>

```

### 4. <table>

### 5. <a>

### 6. <img>

### 7. <>