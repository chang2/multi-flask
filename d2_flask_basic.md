# Flask basic

## I. C9


## II. `flask` 설치

## III. hello `flask` 
### 1. Boilerplate
```python
import os
from flask import Flask

app = Flask(__name__) 

@app.route("/")
def hello():
    return "Hello World!"
    
app.run(host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 8080)))
```
### 2. 코드의 이해
#### 1. `import os`
- `os` 모듈을 불러온다.
#### 2. `from flask import Flask`

#### 3. `app = Flask(__name__)`

#### 4. `@app.route("/")`

#### 5. `

#### 6. `app.run(host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 8080)))`
- `app.run(host='IP주소', port='Port')` : 
- `os.getenv()` :
- `Environment Variable` (환경변수) : 
- 환경변수 출력하기

```shell
> echo $IP
> echo $PORT
```

## IV. `flask` 라우팅
### 1. Web의 원리

### 2. simple routing
```python
# 생략

@app.route("/")
def index():
    return "Hello, world!"

@app.route("/john")
def john():
    return "Hello, John!"

# 생략
```

### 3. variable routing
```python
from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    return "Hello, world!"

@app.route("/<name>")
def hello(name):
    return "Hello, {}!".format(name) # String interpolation

```

## V. `flask` view template
