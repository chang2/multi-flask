# Git & Github

## II. Git & Github
### 1. `Git`?
- **버전관리**도구 : 어떻게 우리가 쓸 코드를 관리할 것인가
- **협업**도구 : 어떻게 다른 개발자와 함께 코드를 쓸 것인가
- **배포**도구 : 어떻게 

### 2. 버전관리(Version Control) 도구
#### 1. 왜 필요한가
왜 버전 관리가 필요한가?
- 코드는 끊임없이 수정되고 보완되는 것 (기말 보고서 term paper)
- 기말 보고서의 오타 및 부정확성은 -> 낮은 점수
- 오타 및 부정확한 코드는 -> **서비스가 망함** 
- 따라서, 

#### 2. Git의 기능
1. 상태 변화 추적(diff)

2. 코드 동기화(push, pull)
3. 원본을 잃지 않은 상태에서 변화를 테스트(branch)  

4. 되돌리기(reset, revert) 

#### 3. 협업(Collaboration) 도구
- 코드 동기화(Synchronization)

#### 4. 배포(Deployment) 도구
- 원본을 잃지 않은 상태에서 변화를 테스트 

### 2. `Github`
Git 저장소를 담아둘 수 있는 웹사이트 
1. 개발자들의 페이스북
2. 개발자들의 Google Drive
3. **개발자들의 이력서**

## III. Git 활용하기

### 1. Git 관리 시작(`init`)
- 


### 2. Git 관리할 파일들을 추가(`add`)
- Git으로 관리할 파일들을 git에게 알려주는 것 (*Git은 모든 파일들을 추적, 관리는 선택적으로 가능*)
- 왜? 

### 3. Git 상태 저장(`commit`)
- 현재 코드 상태를 저장하는 방법(스냅샷을 찍는 것, 버전을 남기는 것)
```shell
git commit -m "version 1"
git commit -m "두번째 수정" # 한글도 가능
```
- 대체로 `변화된 내용`을 기록함
```shell
git commit -m "app.py에 코드 추가"
```

### 4. Git 현재 상태(`status`)
- 
```shell
git status
```

### 5. Git 관리 내역 보기(`log`)
- 지금까지 저장된 `commit`들의 내역(history)
- commit 메세지가 중요한 이유 : log를 통해 변화 내역을 한 눈에 알 수 있게 함.

## IV. Github(원격 저장소, remote repository)에 프로젝트 올리기


## V. 정리 
### 6. **Git 사용 패턴 요약**
#### (1) 가장 많이 사용하게 될 `git` 명령어
```shell
git init

git add <파일이름>

git commit -m "커밋 메세지(버전명)"
```

#### (2) 꽤 자주 사용하게 될 `git` 명령어
```shell
git status 

git log

```

## VI. 주의사항
### 1. Git은 폴더 기준 관리

### 2. push된 내용은 수정이 불가능