# 청년취업아카데미 Project 과정

## I. 목차 
- 1일차 : 파이썬 챗봇, 파이썬 Refresher, 웹기초, Git, Static Web(HTML, CSS, Bootstrap)
    - 카카오톡 기반 파이썬 챗봇
    - life hacking, work hacking
    - 프로필 페이지
    - 

- 2일차 : Dynamic Web, Routing, 웹 스크랩/크롤링, API, JSON, 파일저장 
    - fake 검색엔진
    - fake 궁합
    - 
    - 로또 업그레이드(with API)

- 3일차 : CRUD 1, ORM, 파일업로드
    - 게시판 

- 4일차 : CRUD 2 & Auth, session
    - 로그인 구현, 

- 5일차 : Auth, 파일업로드, 배포 
    - fake 인스타그램

## II. 배우게 될 것들
- Python
- HTML5
- CSS
- Git
- Github

## III. Things to 
- 
- 
- 
- 

### IV. 
웹 Application
- Interactive user experience
- Data-driven : uses data to deliver customized contents